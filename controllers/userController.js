import UserModel from "../models/User.js"
import bcrypt from 'bcrypt'
import jwt from 'jsonwebtoken'
import transporter from "../config/emailConfig.js"

class UserController {
    static userRegistration = async (req, res)=>{
        const {name, email, password, passconf, tc} = req.body
        const user =await UserModel.findOne({email: email})
        if(user){
            res.send({"status":"failed", "message":"Email alreadu exists"})
        }else{
            if(name && email && password && passconf && tc){
                if(password===passconf){
                    try{
                        const salt = await bcrypt.genSalt(10)
                        const hashPassword = await bcrypt.hash(password,salt)
                        const newuser = new UserModel({
                            name:name,
                            email:email,
                            password:hashPassword,
                            tc:tc
                        })
                        await newuser.save()
                        const saved_user = await UserModel.findOne({email:email})
                        //generate jwt Token
                        const token = jwt.sign({ userID:saved_user._id}, process.env.JWT_SECRET_KEY, { expiresIn:'5d'})
                        res.status(201).send({"status":"success", "message":" User Registerd Successfully", "token":token})
                    } catch(err){
                        res.send({"status":"failed", "message":"unable to register"})
                    }
                   
                }else{
                    res.send({"status":"failed", "message":"password and confirm password doesn't matched"})
                }
            }else{
                res.send({"status":"failed", "message":"All fields are required"})
            }
        }
    }


    static userLogin = async (req, res) =>{
        try{
            const {email, password} = req.body
            if(email && password){
                const user =await UserModel.findOne({email: email})
                if(user != null){
                    const ismatch = await bcrypt.compare(password, user.password)
                    if((user.email === email) && ismatch){
                        //generate jwt Token
                        const token = jwt.sign({userID:user._id}, process.env.JWT_SECRET_KEY, { expiresIn: '5d'})
                        res.send({"status":"success", "message":"Login successfully", "token":token})
                    }else{
                        res.send({"status":"failed", "message":"wrong credential"})
                    }

                }else{
                    res.send({"status":"failed", "message":"you are not registered"})
                }
            }else{
                res.send({"status":"failed", "message":"All fields are required"})
            }
        }catch(error){
            console.log(error)
            res.send({"status":"failed", "message":"Unable to login"})
        }
    }

    static changeUserPassword = async(req, res) => {
       
            const {password, confirmPassword} = req.body
            if(password && confirmPassword){
                if(password !== confirmPassword){
                    res.send({"staus":"failed","message":"password and confirm password does not matched"})
                }else{
                    const salt = await bcrypt.genSalt(10)
                    const newHashPassword = await bcrypt.hash(password,salt)
                    //console.log(req.user) //debugging
                    await UserModel.findByIdAndUpdate(req.user._id, { $set: { password:newHashPassword } })
                    res.send({"staus":"success","message":"password changed successfully"})
                }
            }else{
                res.send({"staus":"failed","message":"All fields are requird"})
            }
       
    }


    static loggedUser = async (req, res) =>{
        res.send({"user": req.user})
    }

    static  sendPasswordResetEmail = async(req, res) => {
        const { email } = req.body
        if(email){
            const user = await UserModel.findOne ({email :email})
           
            if(user){
                const secret = user._id + process.env.JWT_SECRET_KEY
                const token = jwt.sign({userID:user._id}, secret, {expiresIn:'10m'})
                const link = `http://localhost:3000/api/user/reset/${user._id}/${token}`
                //api/user/reset/:id/:token //react route example 
               // console.log(link);

               //send email
                let info = await transporter.sendMail({
                    from:process.env.EMAIL_FROM,
                    to:user.email,
                    subject:"GeekShop - Password Reset Link",
                    html:`<a href = ${link}>Click here</a> to reset your password`
                })
                res.send({"staus":"success","message":"Password Reset Email sent...Please check Your email fields are requird","info":info})
            }else{
                res.send({"staus":"failed","message":"This Email is not Registered"})
            }
        }else{
            res.send({"staus":"failed","message":"Email requird"})
        }
    }

    static resetPassword = async (req, res) =>{
        const {password, confirmPassword} = req.body    //body = comes from form
        const {id, token} = req.params  //params = comes with url
        const user = await UserModel.findById(id)
        const new_secret = user._id + process.env.JWT_SECRET_KEY

        try{
            jwt.verify(token, new_secret)
            if(password && confirmPassword){
                if(password !== confirmPassword){
                    res.send({"staus":"failed","message":"password and confirm password must be same"})
                } else {
                    const salt = await bcrypt.genSalt(10)
                    const newHashPassword = await bcrypt.hash(password,salt)
                    await UserModel.findByIdAndUpdate(user._id, {$set:{password:newHashPassword}})
                    res.send({"staus":"success","message":"password updated successfuly"})
                }
            } else {
                res.send({"staus":"failed","message":"All Fields are Required"})
            }
        }catch (error){
            res.send({"staus":"failed","message":"Invalid token"})
        }
        
    }

}


export default UserController