import jwt from 'jsonwebtoken'
import UserModel from '../models/User.js'

const checkUserAuth = async(req, res, next) => {
    let token
    const { authorization } = req.headers
    if(authorization && authorization.startsWith('Bearer')) {
        try{
            //get token from header/user side
            token = authorization.split(' ')[1]
            //console.log("Token", token);  //debugging
            //console.log("Authorization", authorization);  //debugging

            //verify token
            const { userID }  = jwt.verify(token, process.env.JWT_SECRET_KEY)
            //console.log(userID)   //debugging

            //get User from token
            req.user = await UserModel.findById(userID).select('-password')
            console.log(req.user._id) //debugging

            next()
        }catch(error){
            console.log(error)
            res.status(401).send({"status":"failed", "message":"Unauthorized User"})
        }
    }
    if(!token){
        res.status(401).send({"status":"failed", "message":"Unauthorized User, No Token"})
    }
}
export default checkUserAuth