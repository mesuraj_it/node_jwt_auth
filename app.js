//const dotenv = require('dotenv');
import dotenv from 'dotenv'
dotenv.config()
//const express = require('express');
import express from 'express'
//const cors = require('cors');
import cors from 'cors'
//const connectDB = require('./config/connectdb');
import connectDB from './config/connectdb.js'
import userRoutes from './routes/userRoutes.js'
const app = express()
const port = process.env.port
app.use(cors())

connectDB();

//JSON
app.use(express.json())


//Load Routes
app.use("/api/user", userRoutes)

app.listen(port, () => {
    console.log(`Server is running on http://localhost:${port}`)
})